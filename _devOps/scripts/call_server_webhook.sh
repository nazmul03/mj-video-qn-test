#!/usr/bin/env bash

ENV=$1

if [[ ${ENV} == 'production' ]]
then 
    echo "Setting Production Environemtn"
    ENV_IP=$PROD_ENV_IP
    ENV_PORT=$PROD_ENV_PORT
    ENDPOINT=$PROD_HOOK_ENDPOINT
else 
    echo "Setting Test Environment"
    ENV_IP=$TEST_ENV_IP
    ENV_PORT=$TEST_ENV_PORT
    ENDPOINT=$TEST_HOOK_ENDPOINT
fi 

set -e

echo "Making a webhook get request to: http://${ENV_IP}:${ENV_PORT}/hooks/${ENDPOINT}"

curl -o /dev/null -s -w "%{http_code}\n" http://${ENV_IP}:${ENV_PORT}/hooks/${ENDPOINT}