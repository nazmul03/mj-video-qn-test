import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import Navbar from './containers/Navbar';
import TitlePage from './containers/TitlePage';
import QuestionPage from './containers/QuestionPage';
import ErrorPage from './containers/ErrorPage';

const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path='/' component={TitlePage} />
        <Route exact path='/form/:eventId' component={TitlePage} />
        <Route exact path='/form/:eventId/collaborator/:collaboratorId/steps/:stepNo' component={QuestionPage} />
        <Route exact path='/error' component={ErrorPage} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
