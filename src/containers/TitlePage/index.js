import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import axios from 'axios';

const TitlePage = (props) => {
  const [eventData, setEventData] = React.useState(null);

  React.useEffect(() => {
    axios
      .get(
        `https://api.miljulapp.com/api/v1/api/v1/collab/collabevent/${
          props && props.match && props.match.params.eventId
        }`
      )
      .then((response) => {
        console.log(response);
        setEventData(response.data.response.response);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [props]);

  console.log(props, eventData && eventData.meta_data && eventData.meta_data.linked_collab_events[0]);

  return (
    <div
      style={{
        height: '80vh',
        display: 'flex',
        justifyContent: 'center'
      }}
    >
      <div
        className='row'
        style={{
          width: '80vw',
          height: '80vh',
          boxShadow: '2px 2px 10px 10px #eee',
          borderRadius: '20px',
          backgroundColor: '#fff',
          overflow: 'hidden'
        }}
      >
        <div className='col-12 col-sm-6 p-0 m-0'>
          <video
            src={eventData && eventData.meta_data && eventData.meta_data.question}
            autoPlay
            loop
            controls
            style={{ width: '40vw', borderTopLeftRadius: '20px', borderBottomLeftRadius: '20px' }}
          ></video>
          <div
            style={{
              position: 'absolute',
              top: '0',
              left: '0',
              width: '100%',
              padding: '0 1rem'
            }}
          >
            <div style={{ width: 'fit-content', margin: 'auto', fontSize: '2rem', fontWeight: '500', color: '#fff' }}>
              {eventData && eventData.name}
            </div>
          </div>
        </div>
        <div className='col-12 col-sm-auto p-0 m-0'>
          <div
            style={{
              height: '80vh',
              minWidth: '40vw',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <form style={{ width: '80%' }}>
              <div class='form-group'>
                <input
                  type='text'
                  class='form-control'
                  id='name'
                  aria-describedby='nameHelp'
                  placeholder='Enter Name Here'
                />
              </div>
              <div class='form-group'>
                <input
                  type='email'
                  class='form-control'
                  id='email'
                  aria-describedby='emailHelp'
                  placeholder='Enter Email Address Here'
                />
              </div>
              <div style={{ width: 'fit-content', margin: 'auto' }}>
                <Link
                  to={{
                    pathname: `/form/${props && props.match && props.match.params.eventId}/collaborator/269/steps/1`,
                    query: `${eventData && eventData.meta_data && eventData.meta_data.linked_collab_events[0]}`
                  }}
                >
                  Continue
                </Link>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(TitlePage);
