import React from 'react';
import Webcam from 'react-webcam';

const Camera = () => {
  const videoConstraints = {
    aspectRatio: 0.8
  };

  const webcamRef = React.useRef(null);

  return (
    <Webcam
      videoConstraints={videoConstraints}
      audio={true}
      ref={webcamRef}
      mirrored
      style={{ height: '80vh', width: '100%', borderTopLeftRadius: '20px', borderBottomLeftRadius: '20px' }}
    />
  );
};

export default Camera;
