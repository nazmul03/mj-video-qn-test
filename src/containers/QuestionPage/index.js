import React from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import Webcam from 'react-webcam';

const QuestionPage = (props) => {
  const [eventData, setEventData] = React.useState(null);
  const videoConstraints = {
    aspectRatio: 0.8
  };

  const webcamRef = React.useRef(null);

  React.useEffect(() => {
    axios
      .get(
        `https://api.miljulapp.com/api/v1/api/v1/collab/collabevent/${props && props.location && props.location.query}`
      )
      .then((response) => {
        console.log(response);
        setEventData(response.data.response.response);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [props]);

  console.log(props, props.location && props.location.query);

  return (
    <div
      style={{
        height: '80vh',
        display: 'flex',
        justifyContent: 'center'
      }}
    >
      <div
        className='row'
        style={{
          width: '80%',
          height: '80vh',
          boxShadow: '2px 2px 10px 10px #eee',
          borderRadius: '20px',
          backgroundColor: '#fff',
          overflow: 'hidden'
        }}
      >
        <div className='col-12 col-sm-6 p-0 m-0'>
          <video
            src={eventData && eventData.meta_data && eventData.meta_data.question}
            autoPlay
            loop
            controls
            style={{ width: '100%', borderTopLeftRadius: '20px', borderBottomLeftRadius: '20px' }}
          ></video>
          <div
            style={{
              position: 'absolute',
              top: '0',
              left: '0',
              width: '100%',
              padding: '0 1rem'
            }}
          >
            <div style={{ width: 'fit-content', margin: 'auto', fontSize: '2rem', fontWeight: '500', color: '#fff' }}>
              {eventData && eventData.name}
            </div>
          </div>
        </div>
        <div className='col-12 col-sm-6 p-0 m-0'>
          <Webcam
            videoConstraints={videoConstraints}
            audio={true}
            ref={webcamRef}
            mirrored
            style={{ width: '100%', borderTopLeftRadius: '20px', borderBottomLeftRadius: '20px' }}
          />
        </div>
      </div>
    </div>
  );
};

export default withRouter(QuestionPage);
