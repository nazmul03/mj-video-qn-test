import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';

import Logo from '../../assets/images/logo.png';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    padding: '1rem 0'
  }
}));

const Navbar = (props) => {
  console.log(props);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Toolbar>
        <img src={Logo} alt='Logo' width='60' />
      </Toolbar>
    </div>
  );
};

export default Navbar;
